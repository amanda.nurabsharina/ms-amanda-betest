const mongoose = require('mongoose');

const userDataSchema = mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
    },
    userName: {
        type: String,
        required: true,
        unique: true,
    },
    accountNumber: {
        type: String,
    },
    emailAddress: {
        type: String,
    },
    identityNumber: {
        type: String,
        required: true,
        unique: true
    },
})

const userDataModel = mongoose.model('userDataModel', userDataSchema);
module.exports = userDataModel;