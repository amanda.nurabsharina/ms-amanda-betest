const userService = require('../services/user.services');

// get all user //
async function getAll(req, res, next) {
    try {
        const users = await userService.getAll();
        if (users) {
            return res.status(200).json({ status: 200, data: users, message: "SUCCESS" });
        }
        return res.status(404).json({ status: 404, message: "DATA NOT FOUND" });
    } catch (e) {
        console.log(e.toString());
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

// get by account number //
async function getByAccountNumber(req, res, next) {
    if (!req.params.accNumber) {
        return res.status(400).json({ status: 400, message: "BAD PARAMETER" });
    }
    try {
        var user = await userService.getByAccountNumber(req.params.accNumber);
        if (user) {
            return res.status(200).json({ status: 200, data: user, message: "SUCCESS" });
        }
        return res.status(404).json({ status: 404, message: "DATA NOT FOUND" });
    } catch (e) {
        console.log(e.toString());
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

// get by identity number //
async function getByIdentityNumber(req, res, next) {
    if (!req.params.idNumber) {
        return res.status(400).json({ status: 400, message: "BAD PARAMETER" });
    }
    try {
        var user = await userService.getByIdentityNumber(req.params.idNumber);
        if (user) {
            return res.status(200).json({ status: 200, data: user, message: "SUCCESS" });
        }
        return res.status(404).json({ status: 404, message: "DATA NOT FOUND" });
    } catch (e) {
        console.log(e.toString());
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

// create new user //
async function createUser(req, res, next) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).json({ status: 400, message: "BAD PARAMETER" });
    }
    try {
        var user = await userService.createUser(req.body);
        if (user == true) {
            return res.status(201).json({ status: 201, message: "SUCCESSFULLY INSERTED" });
        }
        return res.status(400).json({ status: 400, message: "FAILED TO INSERT" });
    } catch (e) {
        console.log(e.toString());
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

// update existing user //
async function updateUser(req, res, next) {
    if (!req.params.id || req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res.status(400).json({ status: 400, message: "BAD PARAMETER" });
    }
    try {
        var user = await userService.updateUser(req.params.id, req.body);
        if (user == true) {
            return res.status(200).json({ status: 201, message: "SUCCESSFULLY UPDATED" });
        }
        return res.status(400).json({ status: 400, message: 'FAILED TO UPDATE' });
    } catch (e) {
        console.log(e.toString())
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

// delete user by id //
async function deleteUser(req, res, next) {
    if (!req.params.id) {
        return res.status(400).json({ status: 400, message: "BAD PARAMETER" });
    }
    try {
        var user = await userService.deleteUser(req.params.id);
        if (user == true) {
            return res.status(200).json({ status: 200, message: "SUCCESSSFULLY DELETED" });
        }
        return res.status(400).json({ status: 400, message: 'FAILED TO DELETE' });
    } catch (e) {
        console.log(e.toString());
        return res.status(400).json({ status: 500, message: 'INTERNAL SERVER ERROR' });
    }
}

module.exports = {
    getAll,
    getByAccountNumber,
    getByIdentityNumber,
    deleteUser,
    updateUser,
    createUser,
}