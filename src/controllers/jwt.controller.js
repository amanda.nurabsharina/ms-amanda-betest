require('dotenv').config();
const { signJwtToken } = require('../utils/jwt');

// generate token
const tokenHandler = async () => {
    const data = {name : process.env.USERNAME, password : process.env.PASSWORD};
    try {
      const accessToken = await signJwtToken(data, {
        secret: process.env.JWT_ACCESS_TOKEN_SECRET,
        expiresIn: process.env.JWT_EXPIRY
      })
      return Promise.resolve(accessToken)
    } catch (error) {
      return Promise.reject(error)
    }
  }

async function generateToken(req, res, next) {
    try { 
        const token = await tokenHandler();
        if (token == null) {
            console.log(token);
            return res.status(400).json({status: 400, message: "FAILED TO GENERATE TOKEN, INVALID CREDENTIAL"});
        }
        return res.status(200).json({ status: 200, token : token });
      } catch (error) {
          next(error.toString());
          return res.status(500).json({ status: 500, message : "INTERNAL SERVER ERROR"});
      }
}

module.exports = {
    generateToken,
}