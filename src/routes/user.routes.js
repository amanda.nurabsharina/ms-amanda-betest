module.exports = app => {
    
    const userController = require('../controllers/user.controllers');  
    const jwtController = require('../controllers/jwt.controller');
    const { accessTokenValidator } = require('../middlewares/auth')
    var router = require("express").Router();
    
    router.get('/', accessTokenValidator, userController.getAll);
    router.get('/accNumber/:accNumber', accessTokenValidator, userController.getByAccountNumber);
    router.get('/idNumber/:idNumber', accessTokenValidator, userController.getByIdentityNumber);
    router.post('/', accessTokenValidator, userController.createUser);
    router.patch('/:id', accessTokenValidator, userController.updateUser);
    router.delete('/:id', accessTokenValidator, userController.deleteUser);
    router.get('/token', jwtController.generateToken);

    app.use("/api/users", router);
}