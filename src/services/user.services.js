const userData = require('../models/user.model');

async function getAll() {
    try {
        var users = await userData.find()
        // .cache({ expire: 10 });;
        if (users.length == 0) {
            return null;
        }
        return users;
    } catch (e) {
        console.log(e.toString());
        throw e;
    }
}

async function getByAccountNumber(accNumber) {
    try {
        var user = await userData.find({ accountNumber: accNumber });
        if (user.length == 0) {
            return null;
        } 
        return user;
    } catch (e) {
        console.log(e.toString());
        throw e;
    }
}

async function getByIdentityNumber(idNumber) {
    try {
        var user = await userData.find({ identityNumber: idNumber });
        if (user.length == 0) {
            return null;
        }
        return user;
    } catch (e) {
        console.log(e.toString());
        return null;
    }
}

async function createUser(params) {
    try {
        const newUser = new userData(params);
        // await newUser.save().then((v_data) => {
        //     console.log(v_data);
        //     clearCache(v_data.vehicleType)
        // });
        return true;
    } catch (e) {
        console.log(e.toString());
        return false;
    }

}

async function updateUser(id, dataUpdate) {
    try {
        const idUpdate = { id : id };
        var user = await userData.findOneAndUpdate(
            idUpdate, dataUpdate,
            {
                returnOriginal: true
            });
        console.log (user);
        if (!user) {
            return false;
        }
        return true;

    } catch (e) {
        console.log(e.toString());
        return false;
    }

}

async function deleteUser(id) {
    try {
        var user = await userData.findOneAndRemove({ id: id });
        console.log(user);
        if (!user) {
            return false;
        }
        return true;
    } catch (e) {
        console.log(e.toString());
        return false;
    }
}

module.exports = { 
    getAll,
    getByAccountNumber,
    getByIdentityNumber,
    createUser,
    deleteUser,
    updateUser
 };