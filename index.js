const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();
// require('./src/cache/cache');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//connect to db
mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true,useUnifiedTopology: true } );
var db = mongoose.connection;
db.on('error', ()=>{
    console.log('Unable to connect MongoDB!');
});
db.once('open', ()=> {
    console.log('Connected to mongoDB!');
});

app.get('/', (req, res) => {
  res.json({ message: 'Welcome!!' });
});

require('./src/routes/user.routes')(app);

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
