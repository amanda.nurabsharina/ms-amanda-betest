const expect = require('chai').expect;
const userService = require('../src/services/user.services');
const userData = require('../src/models/user.model')
const assert = require('assert');

describe('userService.getAll() Test', () => {
    it('should return userObject', () => {
        const result = userService.getAll();
        const expected = new userData();
        assert.deepStrictEqual(result, expected, 'Expect names to be equal.');
    });
});

// describe('getByIdentityNumber test', async () => {
//     describe('userService.getByIdentityNumber Test', () => {
//         it('should return userObject', () => {
//             const result = await userService.getByIdentityNumber('1');
//             const expected = new userData();
//             assert.deepStrictEqual(result, expected, 'Expect names to be equal.');
//         });
//     });
// });

// describe('getByAccountNumber test', async () => {
//     describe('userService.getByAccountNumber Test', () => {
//         it('should return userObject', () => {
//             const result = await userService.getByAccountNumber('1');
//             const expected = new userData();
//             assert.deepStrictEqual(result, expected, 'Expect names to be equal.');
//         });
//     });
// });

// describe('createUser test', async () => {
//     describe('userService.createUser Test', () => {
//         it('should return userObject', () => {
//             const result = await userService.getByAccountNumber('1');
//             const expected = true
//             assert.deepStrictEqual(result, expected, 'Expect names to be equal.');
//         });
//     });
// });